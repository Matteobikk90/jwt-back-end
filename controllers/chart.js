const { db } = require('../config/db/db');

const getCharts = (req, res) => {
  const { id } = req.params;

  db.select('*')
    .from('charts')
    .where('charts.user_id', '=', id)
    .orderBy('user_id', 'asc')
    .then((charts) => res.status(200).json(charts))
    .catch((err) => res.status(400).json(err.message));
};

const addChart = (req, res) => {
  const { title, data, user_id } = req.body;

  db.insert({
    user_id: user_id,
    title: title,
    data: data,
  })
    .into('charts')
    .returning('*')
    .then((addedChart) => res.status(200).json(addedChart[0]))
    .catch((err) => res.status(400).json(err.message));
};

const deleteChart = (req, res) => {
  const { id } = req.body;

  db('charts')
    .where('charts.id', '=', id)
    .del()
    .returning('*')
    .then((chart) => res.status(200).json(chart))
    .catch((err) => res.status(400).json(err.message));
};

const updateChart = (req, res) => {
  const { title, data } = req.body;
  const { id } = req.params;
  db('charts')
    .where('charts.id', '=', id)
    .update({
      title: title,
      data: data,
    })
    .returning('*')
    .then((chart) => res.status(200).json(chart))
    .catch((err) => res.status(400).json(err.message));
};

module.exports = {
  getCharts: getCharts,
  addChart: addChart,
  deleteChart: deleteChart,
  updateChart: updateChart,
};
