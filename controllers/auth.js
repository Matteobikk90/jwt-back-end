const config = require('../config/auth.config');
const { db } = require('../config/db/db');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

const signup = (req, res) => {
  const { email, username, password } = req.body;

  db.insert({
    username: username,
    email: email,
    password: bcrypt.hashSync(password, 8),
    created_on: new Date(),
  })
    .into('users')
    .returning('*')
    .then((user) => {
      res.status(200).json(user).send('User was registered successfully!');
    })
    .catch((err) => {
      res.status(500).json({ message: err.message });
    });
};

const signin = (req, res) => {
  db.select('*')
    .from('users')
    .where('username', '=', req.body.username)
    .then((user) => {
      if (user[0] === undefined) {
        return res.status(404).send({ message: 'User Not found.' });
      }

      const passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user[0].password
      );

      if (!passwordIsValid) {
        return res.status(401).send({
          accessToken: null,
          message: 'Invalid Password!',
        });
      }

      const token = jwt.sign({ id: user.user_id }, config.secret, {
        expiresIn: 86400, // 24 hours
      });

      res.status(200).send({
        id: user[0].user_id,
        username: user[0].username,
        email: user[0].email,
        accessToken: token,
      });
    })
    .catch((err) => res.status(500).send({ message: err.message }));
};

module.exports = {
  signin: signin,
  signup: signup,
};
