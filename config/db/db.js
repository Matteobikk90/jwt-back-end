const knex = require('knex');

const db = knex({
  client: 'pg',
  connection: {
    host: '127.0.0.1',
    user: 'matteosoresini',
    password: 'development',
    database: 'full-stack-charts',
  },
});

module.exports = {
  db: db,
};
