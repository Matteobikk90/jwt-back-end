const controller = require('../controllers/chart');

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header(
      'Access-Control-Allow-Headers',
      'x-access-token, Origin, Content-Type, Accept'
    );
    next();
  });

  app.get('/api/getCharts/:id', controller.getCharts);
  app.post('/api/addChart/', controller.addChart);
  app.delete('/api/deleteChart/', controller.deleteChart);
  app.put('/api/updateChart/:id', controller.updateChart);
};
